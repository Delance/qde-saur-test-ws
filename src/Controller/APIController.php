<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Classe avec les API pour le site public (appels derrière les formulaires)
 *
 * @Route("/api/v1")
 */
class APIController extends AbstractController
{

    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/ping", name="ping", methods={"GET", "POST"})
     */
    public function pingAction(Request $request)
    {
        $this->logger->info("ping");

        if ($request->getMethod() == Request::METHOD_GET) {
            $result['code'] = '200';
            $result['message'] = '';
            // $result['code'] = '418';
            // $result['message'] = 'I\'m a teapot (error msg test)';

            return new JsonResponse($result);
        } else {
            $result['code'] = '500';
            $result['message'] = 'Invalid method';

            return new JsonResponse($result, 200);
        }
    }

    /**
     * @Route("/contact-simple/upload", name="contact-simple", methods={"POST"})
     */
    public function contactSimpleAction(Request $request)
    {
        $this->logger->info('contact-simple');
        $this->logger->info($request->getContent());

        $result['code'] = '200';
        $result['message'] = '';
        // $result['code'] = '500';
        // $result['message'] = 'Message d\'erreur contact';

        return new JsonResponse($result, 200);
    }


    /**
     * @Route("/contact/upload", name="contact", methods={"POST"})
     */
    public function contactClienteleAction(Request $request)
    {
        $this->logger->info('contact (clientele)');
        $this->logger->info($request->getContent());

        $result['code'] = '200';
        $result['message'] = '';
        // $result['code'] = '500';
        // $result['message'] = 'Message d\'erreur contact';

        return new JsonResponse($result, 200);
    }

    /**
     * @Route("/abonnement/upload", name="abonnement", methods={"POST"})
     */
    public function abonnementAction(Request $request)
    {
        $this->logger->info('abonnement');
        $this->logger->info($request->getContent());

        $result['code'] = '200';
        $result['message'] = '';
        // $result['code'] = '500';
        // $result['message'] = 'Message d\'erreur abonnement';

        return new JsonResponse($result, 200);
    }

    /**
     * @Route("/branchement/upload", name="branchement", methods={"POST"})
     */
    public function branchementAction(Request $request)
    {
        $this->logger->info('branchement');
        $this->logger->info($request->getContent());

        $result['code'] = '200';
        $result['message'] = '';
        // $result['code'] = '500';
        // $result['message'] = 'Message d\'erreur branchement';

        return new JsonResponse($result, 200);
    }
}
