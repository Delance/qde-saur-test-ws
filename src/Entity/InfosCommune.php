<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InfosCommuneRepository")
 */
class InfosCommune
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Groups({"saur"})
     */
    private $prix;


    /**
     * @ORM\Column(type="float")
     * @Groups({"saur"})
     */
    private $calcaire;

    /**
     * @OneToOne(targetEntity="Commune")
     * @JoinColumn(name="commune_id", referencedColumnName="id")
     */
    private $commune;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param mixed $prix
     */
    public function setPrix($prix): void
    {
        $this->prix = $prix;
    }

    /**
     * @return mixed
     */
    public function getCalcaire()
    {
        return $this->calcaire;
    }

    /**
     * @param mixed $calcaire
     */
    public function setCalcaire($calcaire): void
    {
        $this->calcaire = $calcaire;
    }

    /**
     * @return mixed
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * @param mixed $commune
     */
    public function setCommune($commune): void
    {
        $this->commune = $commune;
    }
}
