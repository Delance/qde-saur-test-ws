<?php

namespace App\Repository;

use App\Entity\ParametreQualite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ParametreQualite|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParametreQualite|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParametreQualite[]    findAll()
 * @method ParametreQualite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParametreQualiteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ParametreQualite::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('q')
            ->where('q.something = :value')->setParameter('value', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
