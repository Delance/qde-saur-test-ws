#!/bin/bash

# Script used to init docker image used during CI, after checking the base PHP image
# Taken from: https://docs.gitlab.com/ee/ci/examples/php.html

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install base packages
apt-get update -yqq
apt-get install git wget zip unzip -yqq
# Not possible on php:7.3 as PHP Debian packages have been removed
# due to /etc/apt/preferences.d/no-debian-php
# https://github.com/laradock/laradock/issues/1476
# apt-get install php-intl php-json php-gd -yqq

# Install phpunit, the tool that we will use for testing
# curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
# chmod +x /usr/local/bin/phpunit

# Install composer dependencies
wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php'); unlink('installer.sig');"
mv composer.phar /usr/local/bin/composer
chmod 755 /usr/local/bin/composer
